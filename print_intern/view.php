<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$intern = $_SESSION['intern'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table>
        <tr>
            <td>Ten</td>
            <td><?= $intern['ten'] ?></td>
        </tr>
        <tr>
            <td>Ngay Sinh</td>
            <td><?= $intern['ngaysinh'] ?></td>
        </tr>
        <tr>
            <td>Dia chi</td>
            <td><?= $intern['diachi'] ?></td>
        </tr>
        <tr>
            <td>Gioi tinh</td>
            <td><?= $intern['sex'] ?></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><?= $intern['email'] ?></td>
        </tr>
        <tr>
            <td>So dien thoai</td>
            <td><?= $intern['sdt'] ?></td>
        </tr>
    </table>
    <a href="index.php">Home</a>
</body>

</html>