<?php
//tên, ngày sinh, giới tính, địa chỉ, email, số điện thoại
class Internship
{
    protected $ten;
    protected $ngaySinh;
    protected $sex;
    protected $diaChi;
    protected $email;
    protected $sdt;

    // public function __construct($ten, $ngaySinh, $sex, $diaChi, $email, $sdt)
    // {
    // }

    public function getTen()
    {
        return $this->ten;
    }

    public function setTen($ten)
    {
        $this->ten = $ten;

        return $this;
    }

    public function getNgaySinh()
    {
        return $this->ngaySinh;
    }

    public function setNgaySinh($ngaySinh)
    {
        $this->ngaySinh = $ngaySinh;

        return $this;
    }

    public function getSex()
    {
        return $this->sex;
    }

    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    public function getDiaChi()
    {
        return $this->diaChi;
    }

    public function setDiaChi($diaChi)
    {
        $this->diaChi = $diaChi;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getSdt()
    {
        return $this->sdt;
    }

    public function setSdt($sdt)
    {
        $this->sdt = $sdt;

        return $this;
    }

    public function printInfo()
    {
        echo "Ten: " . $this->getTen() . "<br>";
        echo "Dia Chi: " . $this->getDiaChi() . "<br>";
        echo "Ngay Sinh: " . $this->getNgaySinh() . "<br>";
        echo "Gioi Tinh: " . $this->getSex() . "<br>";
        echo "So Dien Thoai: " . $this->getSdt() . "<br>";
        echo "Email: " . $this->getEmail() . "<br>";
        $intern = [
            "ten" => $this->getTen(),
            "diachi" => $this->getDiaChi(),
            "ngaysinh" => $this->getNgaySinh(),
            "sex" => $this->getSex(),
            "sdt" => $this->getSdt(),
            "email" => $this->getEmail()
        ];
        return $intern;
    }
}