<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
include "internship.php";
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $intern = new Internship();
    $intern->setTen($_POST['ten']);
    $intern->setNgaySinh($_POST['ngaysinh']);
    $intern->setSex($_POST['sex']);
    $intern->setDiaChi($_POST['diachi']);
    $intern->setEmail($_POST['email']);
    $intern->setSdt($_POST['sdt']);
    $intern->printInfo();
    $_SESSION['intern'] = $intern->printInfo();
    header("Location: view.php");
}