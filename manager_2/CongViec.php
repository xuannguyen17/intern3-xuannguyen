<?php
include_once "pdo.php";
class CongViec
{
    public static function getAll()
    {
        $sql = "SELECT * FROM magento2.congviec";
        $result = pdo()->query($sql);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $all = $result->fetchAll();
        return $all;
    }

    public static function getById($id)
    {
        $sql = "SELECT * FROM `magento2`.`congviec` WHERE (`id` = '$id')";
        $result = pdo()->query($sql);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $all = $result->fetchAll();
        return $all;
    }

    public static function add($data)
    {
        $insert = [];
        foreach ($data as $key => $values) {
            $insert[] = "`{$key}` = '{$values}'";
        }
        $insert = implode(",", $insert);
        $sql = "INSERT INTO `magento2`.`congviec` SET $insert";
        pdo()->query($sql);
    }

    public static function update($data, $id)
    {
        $insert = [];
        foreach ($data as $key => $values) {
            $insert[] = "`{$key}` = '{$values}'";
        }
        $insert = implode(",", $insert);
        $sql = "UPDATE `magento2`.`congviec` SET $insert WHERE (`id` = '$id');";
        pdo()->query($sql);
    }
    public static function delete($id)
    {
        $sql = "DELETE from `magento2`.`congviec` WHERE (`id` = '$id');";
        pdo()->query($sql);
    }
    public static function findNVById($id)
    {
        $sql = "SELECT * FROM `magento2`.`congviec` WHERE (`idnv` = '$id')";
        $result = pdo()->query($sql);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $all = $result->fetchAll();
        return $all;
    }
}