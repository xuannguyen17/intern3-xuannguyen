<?php
include_once "pdo.php";
class NhanVien
{
    protected $name;
    protected $age;
    protected $address;
    protected $birthDay;
    protected $yearsExp;
    protected $basicSalary;
    protected $level;
    protected $language;
    protected $salary;

    public function __construct()
    {
    }

    public static function getAll()
    {
        $sql = "SELECT * FROM magento2.nhanvien";
        $result = pdo()->query($sql);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $all = $result->fetchAll();
        return $all;
    }

    public static function count()
    {
        $sql = "SELECT COUNT(*) FROM magento2.nhanvien";
        $result = pdo()->query($sql);
        $count = $result->fetchColumn();
        return $count;
    }

    public static function getById($id)
    {
        $sql = "SELECT * FROM `magento2`.`nhanvien` WHERE (`id` = '$id')";
        $result = pdo()->query($sql);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $all = $result->fetch();
        return $all;
    }
    public static function getByVitri($vitri)
    {
        $sql = "SELECT * FROM `magento2`.`nhanvien` WHERE (`vitri` = '$vitri')";
        $result = pdo()->query($sql);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $all = $result->fetchAll();
        return $all;
    }
    public static function getByName($name)
    {
        $sql = "SELECT * FROM `magento2`.`nhanvien` WHERE (`name` like '%$name%')";
        $result = pdo()->query($sql);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $all = $result->fetchAll();
        return $all;
    }

    public static function add($data)
    {
        $insert = [];
        foreach ($data as $key => $values) {

            if (is_array($values)) {
                $values = implode(" ", $values);
            }
            $insert[] = "`{$key}` = '{$values}'";
        }
        $insert = implode(",", $insert);
        $sql = "INSERT INTO `magento2`.`nhanvien` SET $insert";
        pdo()->query($sql);
        // $sql = "INSERT INTO `magento2`.`nhanvien` (name, age, address, vitri, birthDay, yearsExp, basicSalary, level, code) values (?, ?, ?, ?, ?, ?, ?, ?, ?)';";
    }

    public static function update($data, $id)
    {
        $insert = [];
        foreach ($data as $key => $values) {

            if (is_array($values)) {
                $values = implode(" ", $values);
            }
            $insert[] = "`{$key}` = '{$values}'";
        }
        $insert = implode(",", $insert);
        $sql = "UPDATE `magento2`.`nhanvien` SET $insert WHERE (`id` = '$id');";
        pdo()->query($sql);
    }
    public static function delete($id)
    {
        $sql = "DELETE from `magento2`.`nhanvien` WHERE (`id` = '$id');";
        pdo()->query($sql);
    }

    public static function pagination($start, $limit)
    {
        $sql = "SELECT * FROM magento2.nhanvien  LIMIT $start, $limit";
        $result = pdo()->query($sql);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $all = $result->fetchAll();
        return $all;
    }
}