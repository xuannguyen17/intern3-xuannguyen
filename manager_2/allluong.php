<?php
include "NhanVien.php";
include "CongViec.php";
include "HeSoLuong.php";
$allNV = NhanVien::getAll();
$allCV = CongViec::getAll();
$allHS = HeSoLuong::getAll();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <style>
    table,
    th,
    td {
        border: 1px solid black;
    }
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.css" />

    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.js"></script>

</head>

<body>
    <a href="index.php">Home</a>
    <table id="example">
        <thead>
            <tr>
                <th>Ten</th>
                <th>Thang</th>
                <th>Nam</th>
                <th>So Gio Lam Viec</th>
                <th>Tong Luong</th>
                <th>Hanh dong</th>
            </tr>
        </thead>
        <?php foreach ($allNV as $nv) : ?>
        <?php $cv = CongViec::findNVById($nv->id);
            $hs = HeSoLuong::getByLevel($nv->level); ?>
        <?php foreach ($cv as $cv) : ?>
        <tbody>
            <td><?= $nv->name ?></td>
            <td><?= $cv->thang ?></td>
            <td><?= $cv->nam ?></td>
            <td><?= $cv->sogio ?></td>
            <?php if ($nv->level == 'Junior') : ?>
            <td><?= $nv->basicsalary + $cv->sogio * 50000 * $hs->heso ?></td>
            <?php elseif ($nv->level == 'Fresher') : ?>
            <td><?= $nv->basicsalary + $cv->sogio * 50000 * $hs->heso ?></td>
            <?php elseif ($nv->level == 'PM') : ?>
            <td><?php $to = $nv->basicsalary + (30000 + $cv->sogio * 50000) * $hs->heso;
                            echo $to; ?></td>
            <?php elseif ($nv->level == 'BA') : ?>
            <td><?= $nv->basicsalary + (30000 + $cv->sogio * 50000) * $hs->heso ?></td>
            <?php endif ?>
            <td>Hanh dong</td>
            <?php endforeach ?>
        </tbody>
        <?php endforeach ?>
    </table>
    <script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
    </script>
</body>

</html>