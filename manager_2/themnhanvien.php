<?php
include "NhanVien.php";
$staffModel = new NhanVien;
$data = [];
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $datanv = (['name', 'age', 'address', 'vitri', 'level', 'birthday', 'code', 'basicsalary', 'yearsexp']);
    foreach ($datanv as $key) {
        if (isset($_REQUEST[$key])) {
            $data[$key] = $_REQUEST[$key];
        }
    }
    //$set = (['name' = '$_REQUEST['name']', 'age' = '$_REQUEST['age']', 'address' = '$_REQUEST['address']', 'vitri', 'level', 'birthday', 'code', 'basicsalary', 'yearsexp']);
}
try {
    //print_r($data);
    NhanVien::add($data);
    $insert = true;
} catch (Exception $e) {
    $insert = false;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <a type="button" href="index.php">Home</a>
    <form action="" method="post">
        <table>
            <tr>
                <th>Ten</th>
                <td><input name="name"></td>
            </tr>
            <th>Tuoi</th>
            <td><input name="age" type="number"></td>
            </tr>
            <th>Vi tri</th>
            <td>
                <div>
                    <div>
                        <input type="radio" name="vitri" value="Developer">Developer

                        <div>Level
                            <input type="radio" name="level" value="Fresher">
                            <label> Fresher</label>
                            <input type="radio" name="level" value="Junior">
                            <label> Junior</label><br>
                        </div>
                        </input>
                        <input type="radio" name="vitri" value="Manager">Manager
                        <div> Level
                            <input type="radio" name="level" value="PM">
                            <label> PM</label>
                            <input type="radio" name="level" value="BA">
                            <label> BA</label><br>
                        </div>
                        </input>
                    </div>
                </div>
            </td>
            </tr>
            <!-- <th>Level</th>
            <td>
                <div> <input type="radio" name="level" value="Fresher">
                    <label> Fresher</label><br>
                    <input type="radio" name="level" value="Junior">
                    <label> Junior</label><br>
                    <input type="radio" name="level" value="PM">
                    <label> PM</label><br>
                </div>
            </td> -->
            </tr>
            <th>Dia Chi</th>
            <td><input name="address"></td>
            </tr>
            <th>Ngay Sinh</th>
            <td><input name="birthday" type="date"></td>
            </tr>
            <th>Luong Co Ban</th>
            <td><input name="basicsalary" type="number"></td>
            </tr>
            <th>Nam Kinh Nghiem</th>
            <td><input name="yearsexp" type="number"></td>
            </tr>
            <th>Ngon ngu</th>
            <td>
                <div> <input type="checkbox" name="code[]" value="Java">
                    <label> Java</label><br>
                    <input type="checkbox" name="code[]" value="PHP">
                    <label> PHP</label><br>
                    <input type="checkbox" name="code[]" value="Javascript">
                    <label> Javascript</label><br><br>
                </div>
            </td>
            </tr>
            </tr>
            <button type="submit">Them</button>
        </table>
    </form>
</body>

</html>