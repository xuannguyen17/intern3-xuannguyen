<?php
include 'NhanVien.php';
if (isset($_GET['id'])) {
    $nv = NhanVien::getById($_GET['id']);
    $nvcode = explode(" ", $nv->code);
    $nvc = [];
    foreach ($nvcode as $key => $value) {
        $nvc[$value] = $value;
    }
}
$data = [];
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $datanv = (['name', 'age', 'address', 'vitri', 'level', 'birthday', 'code', 'basicsalary', 'yearsexp']);
    foreach ($datanv as $key) {
        if (isset($_REQUEST[$key])) {
            $data[$key] = $_REQUEST[$key];
        }
    }
    $id = $_POST['idnv'];
    var_dump($data);
    try {
        NhanVien::update($data, $id);
        $update = true;
    } catch (Exception $e) {
        $update = false;
    }
    $nv = NhanVien::getById($_GET['id']);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <a type="button" href="index.php">Home</a>
    <table>
        <form action="" method="post">

            <input type="hidden" name="idnv" value="<?= $nv->id ?>" />
            <tr>
                <th>Ten</th>
                <td><input name="name" value="<?= $nv->name ?>"></td>
            </tr>
            <th>Tuoi</th>
            <td><input name="age" type="number" value="<?= $nv->age ?>"></td>
            </tr>
            <th>Vi tri</th>
            <td>
                <div name="vitri">
                    <select>
                        <option type="radio" name="vitri" value="Developer"
                            <?php if ($nv->vitri == 'Developer') : ?>selected<?php endif ?>>Developer</option>
                        <option type="radio" name="vitri" value="Manager"
                            <?php if ($nv->vitri == 'Manager') : ?>selected<?php endif ?>>Manager
                        </option>
                    </select>
                </div>
            </td>
            </tr>
            <th>Level</th>
            <td>
                <div> <input type="radio" name="level" value="Fresher"
                        <?php if ($nv->level == 'Fresher') : ?>checked<?php endif ?>>
                    <label> Fresher</label><br>
                    <input type="radio" name="level" value="Junior"
                        <?php if ($nv->level == 'Junior') : ?>checked<?php endif ?>>
                    <label> Junior</label><br>
                    <input type="radio" name="level" value="PM" <?php if ($nv->level == 'PM') : ?>checked<?php endif ?>>
                    <label> PM</label><br>
                    <input type="radio" name="level" value="BA" <?php if ($nv->level == 'PM') : ?>checked<?php endif ?>>
                    <label> BA</label><br>
                </div>
            </td>
            </tr>
            <th>Dia Chi</th>
            <td><input name="address" value="<?= $nv->address ?>"></td>
            </tr>
            <th>Ngay Sinh</th>
            <td><input name="birthday" type="date" value="<?= $nv->birthDay ?>"></td>
            </tr>
            <th>Luong Co Ban</th>
            <td><input name="basicsalary" type="number" value="<?= $nv->basicsalary ?>"></td>
            </tr>
            <th>Nam Kinh Nghiem</th>
            <td><input name="yearsexp" type="number" value="<?= $nv->yearsExp ?>"></td>
            </tr>
            <th>Ngon ngu</th>
            <td>
                <div>
                    <input type="checkbox" name="code[]" value="Java"
                        <?php if (in_array('Java', $nvcode)) : ?>checked<?php endif ?>>
                    <label> Java</label><br>
                    <input type="checkbox" name="code[]" value="PHP"
                        <?php if (isset($nvc['PHP'])) : ?>checked<?php endif ?>>
                    <label> PHP</label><br>
                    <input type="checkbox" name="code[]" value="Javascript"
                        <?php if (isset($nvc['Javascript'])) : ?>checked<?php endif ?>>
                    <label> Javascript</label><br><br>
                </div>

            </td>
            </tr>
            <td><button type="submit">Sua</button>
            </td>
            </tr>
        </form>
    </table>
</body>

</html>