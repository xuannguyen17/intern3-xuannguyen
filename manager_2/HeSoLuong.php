<?php
include_once "pdo.php";
class HeSoLuong
{
    protected $level;
    protected $heSo;
    public function __construct()
    {
    }
    public static function getAll()
    {
        $sql = "SELECT * FROM magento2.hesoluong";
        $result = pdo()->query($sql);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $all = $result->fetchAll();
        return $all;
    }

    public static function getById($id)
    {
        $sql = "SELECT * FROM `magento2`.`hesoluong` WHERE (`id` = '$id')";
        $result = pdo()->query($sql);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $all = $result->fetchAll();
        return $all;
    }
    public static function getByLevel($level)
    {
        $sql = "SELECT * FROM `magento2`.`hesoluong` WHERE (`level` = '$level')";
        $result = pdo()->query($sql);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $all = $result->fetch();
        return $all;
    }

    public static function add($data)
    {
        $insert = [];
        foreach ($data as $key => $values) {
            $insert[] = "`{$key}` = '{$values}'";
        }
        $insert = implode(",", $insert);
        $sql = "INSERT INTO `magento2`.`hesoluong` SET $insert";
        pdo()->query($sql);
    }

    public static function update($data, $id)
    {
        $sql = "UPDATE `magento2`.`hesoluong` SET $data WHERE (`id` = '$id');";
        pdo()->query($sql);
    }
    public static function delete($id)
    {
        $sql = "DELETE from `magento2`.`hesoluong` WHERE (`id` = '$id');";
        pdo()->query($sql);
    }
}