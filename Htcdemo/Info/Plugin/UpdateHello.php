<?php

namespace Htcdemo\Info\Plugin;

class UpdateHello
{

    // public function beforeSetWelcomeText(\Htcdemo\Info\Block\Hello $hello, $welcome)
    // {
    //     // $welcome = $hello->getWelcomeText();
    //     echo __METHOD__ . "</br>";
    //     $welcome = "Hello Daddy";
    //     return $welcome;
    // }
    // public function afterSetWelcomeText(\Htcdemo\Info\Block\Hello $hello, $welcome)
    // {
    //     echo __METHOD__ . "</br>";
    //     $welcome = "Hello Mommy";
    //     return $welcome;
    // }

    // public function afterGetWelcomeText(\Htcdemo\Info\Block\Hello $hello)
    // {
    //     echo __METHOD__ . "</br>";
    //     $welcome = "Hello Lady!";
    //     return $welcome;
    // }
    public function aroundGetWelcomeText(\Htcdemo\Info\Block\Hello $hello, callable $proceed)
    {
        $hello->setWelcomeText('Hello World');
        echo __METHOD__ . " - Before proceed() </br>";
        $result = $proceed();
        echo __METHOD__ . " - After proceed() </br>";


        return $result;
    }
}
