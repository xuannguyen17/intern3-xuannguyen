<?php

namespace Htcdemo\Info\Block;


class Result extends \Magento\Framework\View\Element\Template
{
    protected $_infoFactory;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Htcdemo\Info\Model\InfoFactory $infoFactory
    ) {
        $this->_infoFactory = $infoFactory;
        parent::__construct($context);
    }

    public function sayHello()
    {
        return __('Hello World');
    }

    public function getPostCollection()
    {
        $post = $this->_infoFactory->create();
        return $post->getCollection();
    }

    public function getEditAction()
    {
        return $this->getUrl('info/page/update', ['_secure' => true]);
    }
    public function getDeleteAction()
    {
        return $this->getUrl('info/page/delete', ['_secure' => true]);
    }
}
