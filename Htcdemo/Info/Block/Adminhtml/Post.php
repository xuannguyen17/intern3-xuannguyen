<?php

namespace Htcdemo\Info\Block\Adminhtml;

class Post extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'Grid_Demo';
        $this->_controller = 'adminhtml_post';
        parent::_construct();
    }
}
