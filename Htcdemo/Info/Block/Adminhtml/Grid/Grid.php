<?php

namespace Htcdemo\Info\Block\Adminhtml\Post;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $postCollection;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Htcdemo\Info\Model\ResourceModel\Info\CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->postCollection = $collectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _prepareCollection()
    {
        $this->setCollection($this->postCollection->create());
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'index' => 'id',
            ]
        );
        $this->addColumn(
            'email',
            [
                'header' => __('Email'),
                'index' => 'email',
            ]
        );
        $this->addColumn(
            'phone',
            [
                'header' => __('Phone'),
                'index' => 'phone',
            ]
        );
        $this->addColumn(
            'birth',
            [
                'header' => __('Birth'),
                'index' => 'birth',
            ]
        );
        return $this;
    }
}
