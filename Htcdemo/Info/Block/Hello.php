<?php

namespace Htcdemo\Info\Block;

class Hello extends \Magento\Framework\View\Element\Template
{
    protected $welcomeText;

    public function setWelcomeText($welcomeText)
    {
        return $this->welcomeText = $welcomeText;
    }
    public function getWelcomeText()
    {
        return $this->welcomeText;
    }
}
