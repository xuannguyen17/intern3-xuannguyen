<?php

namespace Htcdemo\Info\Block;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\View\Element\Template;
use Htcdemo\Info\Model\InfoFactory;

class Update extends Template
{
    private $infoFactory;

    public function __construct(InfoFactory $infoFactory, Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->infoFactory = $infoFactory;
    }

    public function getFormAction()
    {
        return $this->getUrl('info/page/update', ['_secure' => true]);
    }

    public function getById()
    {
        $id = $this->getRequest()->getParam("id");
        $model = $this->infoFactory->create();
        return $model->load($id);
    }
}
