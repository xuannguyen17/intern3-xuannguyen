<?php

namespace Htcdemo\Info\Model\ResourceModel\Info;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Htcdemo\Info\Model\Info',
            'Htcdemo\Info\Model\ResourceModel\Info'
        );
    }
}
