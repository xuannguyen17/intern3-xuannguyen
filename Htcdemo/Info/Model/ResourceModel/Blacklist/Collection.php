<?php

namespace Htcdemo\Info\Model\ResourceModel\Blacklist;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Htcdemo\Info\Model\Blacklist',
            'Htcdemo\Info\Model\ResourceModel\Blacklist'
        );
    }
}
