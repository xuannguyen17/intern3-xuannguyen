<?php

namespace Htcdemo\Info\Model;

use Magento\Framework\Model\AbstractModel;

class Info extends AbstractModel
{
    protected $_eventPrefix = 'info_model';
    protected function _construct()
    {
        $this->_init('Htcdemo\Info\Model\ResourceModel\Info');
    }
}
