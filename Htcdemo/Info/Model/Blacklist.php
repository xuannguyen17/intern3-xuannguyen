<?php

namespace Htcdemo\Info\Model;

use Magento\Framework\Model\AbstractModel;

class Blacklist extends AbstractModel
{
    protected $_eventPrefix = 'blacklist_model';
    protected function _construct()
    {
        $this->_init('Htcdemo\Info\Model\ResourceModel\Blacklist');
    }
}
