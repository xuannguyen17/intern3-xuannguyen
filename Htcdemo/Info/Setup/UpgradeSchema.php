<?php

namespace Htcdemo\Info\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $tableName = $setup->getTable('info_table');
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            if ($setup->getConnection()->isTableExists($tableName) != true) {
                $table = $setup->getConnection()->newTable($tableName)
                    ->addColumn(
                        'id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'unsigned' => true,
                            'nullable' => false,
                            'primary' => true
                        ],
                        'ID'
                    )
                    ->addColumn(
                        'email',
                        Table::TYPE_TEXT,
                        null,
                        ['nullable' => true, 'default' => ''],
                        'Email'
                    )
                    ->addColumn(
                        'address',
                        Table::TYPE_TEXT,
                        null,
                        ['nullable' => true, 'default' => ''],
                        'Dia Chi'
                    )
                    ->addColumn(
                        'phone',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'nullable' => true, 'default' => '0', 'length' => 12,
                        ],
                        'So Dien Thoai'
                    )
                    ->addColumn(
                        'birth',
                        Table::TYPE_DATE,
                        null,
                        [
                            'nullable' => false
                        ],
                        'Ngay Sinh'
                    )
                    ->setComment('Table Info')
                    ->setOption('type', 'InnoDB')
                    ->setOption('charset', 'utf8');
                $setup->getConnection()->createTable($table);
            }
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $setup->getConnection()
                ->changeColumn(
                    $setup->getTable('info_table'),
                    'birth',
                    'birth',
                    [
                        'type' => Table::TYPE_DATE,
                        'nullable' => true,
                        'comment' => 'Ngay Sinh.'
                    ]
                );
        }
        $setup->endSetup();
    }
}
