<?php

namespace Htcdemo\Info\Controller\Page;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Htcdemo\Info\Model\InfoFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Save extends Action
{
    protected $resultPageFactory;
    protected $infoFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        InfoFactory $infoFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->infoFactory = $infoFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $data = (array)$this->getRequest()->getPost();
            if ($data) {
                $model = $this->infoFactory->create();
                $model->setData($data);
                $model->beforeSave();
                // echo "<pre>";
                // print_r($model->debug());
                // die("dead");
                $model->save();
                $this->messageManager->addSuccessMessage(__("Thanh Cong."));
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __("That Bai."));
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('info/page/result');
        return $resultRedirect;

        // $data = $this->getRequest()->getPost();
        // $id = !empty($data['id']) ? $data['id'] : null;
        // print_r($data);

        // $newData = [
        //     'email' => $data['email'],
        //     'address' => $data['address'],
        //     'phone' => $data['phone'],
        //     'birth' => $data['birth'],

        // ];
        // print_r($newData);

        // $info = $this->infoFactory->create();
        // if ($id) {
        //     $info->load($id);
        //     $this->messageManager->addSuccessMessage(__('Edit thành công'));
        // } else {
        //     $this->messageManager->addSuccessMessage(__('Save thành công.'));
        // }
        // try {
        // echo "<pre>";
        // print_r($info->debug());
        // die("dead");
        //     $info->addData($newData);
        //     $this->_eventManager->dispatch('post_before_save', ['infoData' => $info]);

        //     //$info->save();
        //     $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        //     $resultRedirect->setPath('info/page/result');
        //     return $resultRedirect;
        // } catch (\Exception $e) {
        //     $this->messageManager->addErrorMessage(__('Save thất bại.'));
        // }
    }
}
