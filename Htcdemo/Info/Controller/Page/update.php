<?php

namespace Htcdemo\Info\Controller\Page;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Htcdemo\Info\Model\InfoFactory;

class Update extends Action
{
    protected $pageFactory;

    public function __construct(Context $context, PageFactory $pageFactory)
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }


    public function execute()
    {
        return $this->pageFactory->create();
        //$this->_view->loadLayout();
        //$this->_view->renderLayout();
    }
    // protected $resultPageFactory;

    // private $infoFactory;

    // public function __construct(InfoFactory $infoFactory, Context $context, PageFactory $resultPageFactory)
    // {
    //     parent::__construct($context);
    //     $this->resultPageFactory = $resultPageFactory;
    //     $this->infoFactory = $infoFactory;
    // }

    // public function execute()
    // {
    //     if ($this->isCorrectData()) {
    //         return $this->resultPageFactory->create();
    //     } else {
    //         $this->messageManager->addErrorMessage(__("Record Not Found"));
    //         $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
    //         $resultRedirect->setPath('info/page/result');
    //         return $resultRedirect;
    //     }
    // }

    // public function isCorrectData()
    // {
    //     if ($id = $this->getRequest()->getParam("id")) {
    //         $model = $this->infoFactory->create();
    //         $model->load($id);
    //         if ($model->getId()) {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     } else {
    //         return true;
    //     }
    // }
}
