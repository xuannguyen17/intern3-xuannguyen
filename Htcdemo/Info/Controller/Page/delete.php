<?php

namespace Htcdemo\Info\Controller\Page;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Htcdemo\Info\Model\InfoFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Delete extends Action
{
    protected $resultPageFactory;
    protected $infoFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        InfoFactory $infoFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->infoFactory = $infoFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $id = $this->getRequest()->getParams("id");
            if ($id) {
                $model = $this->infoFactory->create()->load($id);
                $model->delete();
                $this->messageManager->addSuccessMessage(__("Xoa Thanh Cong."));
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __("Xoa That Bai."));
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('info/page/result');
        return $resultRedirect;
    }
}
