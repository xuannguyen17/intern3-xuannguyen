<?php

namespace Htcdemo\Info\Controller\Adminhtml\Blacklist;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Blacklist extends Action
{
    protected $pageFactory;

    public function __construct(Context $context, PageFactory $pageFactory)
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }


    public function execute()
    {
        return $resultPage = $this->pageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Black List'));

        return $resultPage;
    }
}
