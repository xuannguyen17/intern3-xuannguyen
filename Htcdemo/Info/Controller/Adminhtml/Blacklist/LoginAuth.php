<?php

namespace Htcdemo\Info\Controller\Adminhtml\Blacklist;

use Magento\Backend\App\Action;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class LoginAuth extends \Magento\Backend\App\Action
{

    const EMAIL_LIST = "block/blacklist/email_list";

    protected $scopeConfig;

    public function __construct(
        Action\Context $context,
        ScopeConfigInterface  $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function execute()
    {
        $email_list = $this->scopeConfig->getValue(self::EMAIL_LIST, ScopeInterface::SCOPE_STORE);
        echo $email_list;
    }
}
