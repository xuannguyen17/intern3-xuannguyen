<?php


namespace Htcdemo\Info\Controller\News;

use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $_pageFactory;

    public function __construct(Action\Context $context, PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function setTitle($title)
    {
        return $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function execute()
    {
        echo $this->setTitle('Welcome');
        echo $this->getTitle();
        return $this->_pageFactory->create();
    }
}
