<?php

namespace Htcdemo\Info\Observer;

use Magento\Framework\Event\Observer;

class ChangeInfo implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(Observer $observer)
    {
        // $data = $observer->getData('infoData');
        $data = $observer->getEvent()->getDataObject();
        $data->setData('address', 'Số 8, Nguyễn Văn Lộc.');
        // echo "<pre>";
        // print_r($data->debug());
        // die("dead");
        $observer->setData('infoData', $data);
    }
}
