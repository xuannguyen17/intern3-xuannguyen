<?php

namespace Htcdemo\Info\Observer;

class LoginAuth implements \Magento\Framework\Event\ObserverInterface
{

    const EMAIL_LIST = "block/blacklist/email_list";
    protected $_scopeConfig;
    protected $actionFlag;
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\ActionFlag $actionFlag
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->redirect = $context->getRedirect();
        $this->_messageManager = $messageManager;
        $this->actionFlag = $actionFlag;
    }

    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $emails = $this->_scopeConfig->getValue(self::EMAIL_LIST, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $email_list = explode(",", $emails);
        $event = $observer->getEvent();
        $controllerAction = $event->getControllerAction();
        $customer = $controllerAction->getRequest()->getParams();
        $email = $customer['email'];

        if (in_array($email, $email_list)) {
            $this->_messageManager->addErrorMessage('Invalid Email!');
            $controllerAction->getResponse()->setRedirect($this->redirect->getRefererUrl());
            $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
            $this->redirect->redirect($controllerAction->getResponse(), $this->redirect->getRefererUrl());
        }
    }
}
