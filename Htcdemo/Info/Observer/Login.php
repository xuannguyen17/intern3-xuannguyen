<?php

namespace Htcdemo\Info\Observer;

class Login implements \Magento\Framework\Event\ObserverInterface
{

    const EMAIL_LIST = "block/blacklist/email_list";
    protected $_scopeConfig;
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->redirect = $context->getRedirect();
        $this->customerSession = $customerSession;
        $this->_messageManager = $messageManager;
    }

    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $emails = $this->_scopeConfig->getValue(self::EMAIL_LIST, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $email_list = explode(",", $emails);

        $customer = $observer->getEvent()->getCustomer();
        $email = $customer->getEmail();

        if (in_array($email, $email_list)) {
            $customerId = $this->customerSession->getId();
            if ($customerId) {
                $this->_messageManager->addErrorMessage('Invalid Email or Wrong Password!!!');
                $this->customerSession->logout()
                    ->setBeforeAuthUrl($this->redirect->getRefererUrl())
                    ->setLastCustomerId($customerId);
            }
        }
    }
}
