<?php

namespace HTCMage\CustomCategory\Block;

class CustomCategory extends \Magento\Framework\View\Element\Template
{
    protected $_categoryFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collecionFactory
    ) {
        $this->_categoryFactory = $collecionFactory;
        $this->_registry = $registry;
        parent::__construct($context);
    }

    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    public function getCustomCategory()
    {
        $catId = 5; // category id        
        $collection = $this->_categoryFactory
            ->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('entity_id', ['eq' => $catId])
            ->setPageSize(1);

        $catObj = $collection->getFirstItem();
        $catData = $catObj->getData();
        var_dump($catData);
        return $catObj->getPopularCategory();
    }
}
