<?php

namespace HTCMage\OderCount\Observer;

use Magento\Framework\Event\Observer;
use Magento\Customer\Model\CustomerFactory;

class onOrder implements \Magento\Framework\Event\ObserverInterface
{
    protected $_responseFactory;
    protected $customerFactory;

    public function __construct(
        \Magento\Sales\Model\OrderFactory $orderFactory,
        CustomerFactory $customerFactory
    ) {
        $this->_orderFactory = $orderFactory;
        $this->customerFactory = $customerFactory;
    }

    public function execute(Observer $observer)
    {
        $customers = $this->getCustomerCollection()->toArray();
        foreach ($customers as $customer) {
            $model = $this->customerFactory->create();
            $order = count($this->getOrders($customer['entity_id']));
            if (array_key_exists('order_count', $customer)) {
                $customer['order_count'] = $order;
            } else {
                $customer['order_count'] = $order;
            }
            $model->setData($customer);
            $model->save();
        }
    }

    public function getOrders($customer_id)
    {
        $this->orders = $this->_orderFactory->create()->getCollection()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'customer_id',
            $customer_id
        )->setOrder(
            'created_at',
            'desc'
        );
        return $this->orders;
    }
    public function getCustomerCollection()
    {
        return $this->customerFactory->create()->getCollection()
            ->addAttributeToSelect("*")
            ->load();
    }
}
