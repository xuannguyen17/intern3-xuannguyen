<?php

namespace HTCMage\OderCount\Cron;

// use Magento\Framework\App\Action\Action;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetup;

class UpdateCustomerOderCount extends \Magento\Framework\App\Helper\AbstractHelper  implements UpgradeDataInterface
{

    protected $_eavAttribute;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $session,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        EavSetup $eavSetupFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
    ) {
        $this->_session = $session;
        $this->_orderFactory = $orderFactory;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->_eavAttribute = $eavAttribute;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->upgrade();
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $count = count($this->getOrders());
        $setup->startSetup();
        $entityType = $this->eavSetupFactory->getEntityType(\Magento\Customer\Model\Customer::ENTITY);
        $entityTypeId = $entityType['entity_type_id'];
        $attributeId = $this->_eavAttribute->getIdByCode(\Magento\Customer\Model\Customer::ENTITY, 'prefix');
        $this->eavSetupFactory->updateAttribute($entityTypeId, $attributeId, 'order_count', $count, null);
        $setup->endSetup();
    }

    public function getOrders()
    {
        $customerId = $this->_session->getCustomer()->getId();
        $this->orders = $this->_orderFactory->create()->getCollection()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'customer_id',
            $customerId
        )->setOrder(
            'created_at',
            'desc'
        );
        return $this->orders;
    }
}