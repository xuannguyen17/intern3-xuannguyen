<?php

namespace HTCMage\OderCount\Controller\OrderCount;

use Magento\Framework\App\Action\Action;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\CustomerFactory;

class OrderCount extends Action
{

    protected $pageFactory;
    protected $customerFactory;
    protected $customerResource;

    public function __construct(
        Context $context,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        CustomerFactory $customerFactory,
        PageFactory $pageFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        parent::__construct($context);
        $this->_orderFactory = $orderFactory;
        $this->customerFactory = $customerFactory;
        $this->_messageManager = $messageManager;
        $this->pageFactory = $pageFactory;
    }

    public function execute()
    {
        $customers = $this->getCustomerCollection()->toArray();
        foreach ($customers as $customer) {
            $model = $this->customerFactory->create();
            echo $order = count($this->getOrders($customer['entity_id']));
            if (array_key_exists('order_count', $customer)) {
                $customer['order_count'] = $order;
            } else {
                $customer['order_count'] = $order;
            }
            $model->setData($customer);
            $model->save();
        }
        // return $this->pageFactory->create();
        $this->_messageManager->addSuccessMessage('Order Thanh Cong!');
        $this->_redirect('home');
    }

    public function getOrders($customer_id)
    {
        $this->orders = $this->_orderFactory->create()->getCollection()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'customer_id',
            $customer_id
        )->setOrder(
            'created_at',
            'desc'
        );
        return $this->orders;
    }
    public function getCustomerCollection()
    {
        return $this->customerFactory->create()->getCollection()
            ->addAttributeToSelect("*")
            ->load();
    }
}
