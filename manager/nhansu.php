<?php
include "NhanVien.php";
$allnhanvien = NhanVien::getAll();

if (isset($_GET['id'])) {
    NhanVien::delete($_GET['id']);
    $allnhanvien = NhanVien::getAll();
}
if (isset($_GET['vitri'])) {
    $allnhanvien = NhanVien::getByVitri($_GET['vitri']);
} else {
    $allnhanvien = NhanVien::getAll();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <a type="button" href="index.php">Home</a>
    <a type="" href="themnhanvien.php">Them Nhan Vien</a>
    <br>
    Loc nhan vien theo vi tri <span><a href="nhansu.php?vitri=Developer">Developer</a></span> <span><a
            href="nhansu.php?vitri=Manager">Manager</a></span> <span><a href="nhansu.php">Tat Ca</a></span>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Ten</th>
                <th>Tuoi</th>
                <th>Vi tri</th>
                <th>Dia Chi</th>
                <th>Ngay Sinh</th>
                <th>Luong Co Ban</th>
                <th>Nam Kinh Nghiem</th>
                <th>Level</th>
                <th>Ngon ngu</th>
                <th>Hanh dong</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($allnhanvien as $nv) : ?>
            <tr>
                <td><?= $nv->id ?></td>
                <td><?= $nv->name ?></td>
                <td><?= $nv->age ?></td>
                <td><?= $nv->vitri ?></td>
                <td><?= $nv->address ?></td>
                <td><?= $nv->birthday ?></td>
                <td><?= $nv->basicsalary ?></td>
                <td><?= $nv->yearsExp ?></td>
                <td><?= $nv->level ?></td>
                <td><?= $nv->code ?></td>
                <td><a type="button" href="suanhanvien.php?id=<?= $nv->id ?>">Sua</a>
                    <a type="button" href="nhansu.php?id=<?= $nv->id ?>">Xoa</a>
                </td>
            </tr><?php endforeach ?>
        </tbody>
    </table>
</body>

</html>