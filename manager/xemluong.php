<?php
include "NhanVien.php";
include "CongViec.php";
include "HeSoLuong.php";
$allNV = NhanVien::getAll();
$allCV = CongViec::getAll();
$allHS = HeSoLuong::getAll();
if (isset($_GET['id'])) {
    $nvCV = CongViec::findNVById($_GET['id']);
    $nv = NhanVien::getById($_GET['id']);
    $hs = HeSoLuong::getByLevel($nv->level);
}
// if (isset($_POST['giocong'])) {
//     $data = ['idnv' => $_REQUEST['idnv'], 'sogio' => $_REQUEST['sogio'], 'thang' => $_REQUEST['month'], 'nam' => $_REQUEST['year']];
//     //var_dump($data);
//     CongViec::add($data);
//     $nvCV = CongViec::findNVById($_REQUEST['idnv']);
// }
$action = (isset($_GET['action'])) ? $_GET['action'] : '';
$id = (isset($_GET['idcv'])) ? $_GET['idcv'] : "";
if ($action == "update") {
    $id = $_GET['idcv'];
    $data = ['idnv' => $_REQUEST['idnv'], 'sogio' => $_REQUEST['sogio'], 'thang' => $_REQUEST['month'], 'nam' => $_REQUEST['year']];
    //var_dump($data);
    try {
        //var_dump($_GET['idcv']);
        CongViec::update($data, $id);
    } catch (Exception $e) {
        $e->getMessage();
    }
    $nvCV = CongViec::findNVById($_REQUEST['idnv']);
    $nv = NhanVien::getById($_REQUEST['idnv']);
    $hs = HeSoLuong::getByLevel($nv->level);
} elseif ($action == "delete") {
    CongViec::delete($id);
    //var_dump($_GET['idcv']);
    $nvCV = CongViec::findNVById($_REQUEST['idnv']);
    $nv = NhanVien::getById($_REQUEST['idnv']);
    $hs = HeSoLuong::getByLevel($nv->level);
} elseif ($action == "add") {
    $data = ['idnv' => $_REQUEST['idnv'], 'sogio' => $_REQUEST['sogio'], 'thang' => $_REQUEST['month'], 'nam' => $_REQUEST['year']];
    //var_dump($data);
    CongViec::add($data);
    //var_dump($_GET['idnv']);
    $nvCV = CongViec::findNVById($_REQUEST['idnv']);
    $nv = NhanVien::getById($_REQUEST['idnv']);
    $hs = HeSoLuong::getByLevel($nv->level);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <style>
    table,
    th,
    td {
        border: 1px solid black;
    }
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <a href="index.php">Home</a>
    <table>
        <tr>
            <th>Ten Nhan Vien</th>
        </tr><?php foreach ($allNV as $nvs) : ?>
        <tr>
            <td><a href="xemluong.php?id=<?= $nvs->id ?>"><?= $nvs->name ?></a></td>
        </tr><?php endforeach ?>
    </table>
    <br>
    <?php if (isset($nvCV)) : ?>
    <form action="" method="get">
        <input type="hidden" name="idnv" value="<?= $nv->id ?>">
        Thang<input type="number" name="month" min="1" max="12">
        Nam<input type="number" name="year">
        So Gio Lam Viec<input type="number" name="sogio">
        <button type="submit" name="action" value="add">Them Gio Cong</button>
    </form>
    Bang luong cua <span><?= $nv->name ?></span>
    <table>
        <thead>
            <tr>
                <th>Thang</th>
                <th>Nam</th>
                <th>So Gio Lam Viec</th>
                <th>Tong Luong</th>
                <th>Hanh dong</th>
            </tr>
        </thead>
        <tbody>

            <?php foreach ($nvCV as $nvCV) : ?>
            <tr>
                <form action="" method="get">
                    <input type="hidden" name="idcv" value="<?= $nvCV->id ?>">
                    <input type="hidden" name="idnv" value="<?= $nvCV->idnv ?>">
                    <td><input name="month" value="<?= $nvCV->thang ?>"></input></td>
                    <td><input name="year" value="<?= $nvCV->nam ?>"></input></td>
                    <td><input name="sogio" value="<?= $nvCV->sogio ?>"></input></td>
                    <?php if ($nv->level == 'Junior') : ?>
                    <td><?= $nv->basicsalary + $nvCV->sogio * 50000 * $hs->heso ?></td>
                    <?php elseif ($nv->level == 'Fresher') : ?>
                    <td><?= $nv->basicsalary + $nvCV->sogio * 50000 * $hs->heso ?></td>
                    <?php elseif ($nv->level == 'PM') : ?>
                    <td><?php $to = $nv->basicsalary + (30000 + $nvCV->sogio * 50000) * $hs->heso;
                                    echo $to; ?></td>
                    <?php elseif ($nv->level == 'BA') : ?>
                    <td><?= $nv->basicsalary + (30000 + $nvCV->sogio * 50000) * $hs->heso ?></td>
                    <?php endif ?>
                    <td><button type="submit" name="action" value="update">Sua</button>
                        <button type="submit" name="action" value="delete">Xoa</button>
                    </td>
                </form>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    <?php endif ?>
</body>

</html>