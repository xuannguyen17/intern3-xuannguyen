<?php

function pdo()
{
    if (isset($conn)) {
        return;
    } else {
        $conn = null;
        try {
            $conn = new PDO('mysql:localhost;dbname=magento2', 'root', '');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Loi PDO" . $e->getMessage();
        }
        return $conn;
    }
}
// function test()
// {
//     $conn = null;
//     try {
//         $conn = new PDO('mysql:localhost;dbname=magento2', 'root', '');
//         $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//     } catch (PDOException $e) {
//         echo "Loi PDO" . $e->getMessage();
//     }
//     $sql = "SELECT * FROM magento2.nhanvien";
//     $result = $conn->query($sql);
//     $result->setFetchMode(PDO::FETCH_ASSOC);
//     $bill = $result->fetchAll();
//     var_dump($bill);
// }
// test();