<?php
include "HeSoLuong.php";
$allHsLuong = HeSoLuong::getAll();
$action = (isset($_GET['action'])) ? $_GET['action'] : '';
$id = (isset($_GET['id'])) ? $_GET['id'] : "";
$data = [];
if ($action == "update") {
    $id = $_GET['id'];
    $data = ['level' => $_REQUEST['level'], 'heso' => $_REQUEST['heso']];
    //var_dump($data);
    try {
        HeSoLuong::update($data, $id);
    } catch (Exception $e) {
        $e->getMessage();
    }
    $allHsLuong = HeSoLuong::getAll();
} elseif ($action == "delete") {
    HeSoLuong::delete($id);
    $allHsLuong = HeSoLuong::getAll();
} elseif ($action == "add") {
    $data = ['level' => $_REQUEST['level'], 'heso' => $_REQUEST['heso']];
    try {
        HeSoLuong::add($data);
    } catch (Exception $e) {
        $done = false;
    }
    $allHsLuong = HeSoLuong::getAll();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <a href="index.php">Home</a>
    <table>
        <tr>
            <td>Level</td>
            <td>He So Luong</td>
            <td>Hanh Dong</td>
        </tr>

        <?php foreach ($allHsLuong as $hsLuong) : ?>
        <form action="" method="get">
            <tr>
                <input name="id" type="hidden" value="<?= $hsLuong->id ?>"> </input>
                <td><input name="level" value="<?= $hsLuong->level ?>" require></td>
                <input type="hidden" name=level_old" value="<?= $hsLuong->level ?>">
                <td><input name="heso" value="<?= $hsLuong->heso ?>" require></td>
                <input type="hidden" name=heso_old" value="<?= $hsLuong->heso ?>">
                <td><button type="submit" name="action" value="update">Cap nhat</button>
                    <button type="submit" name="action" value="delete">Xoa</button>
                </td>
            </tr>
        </form>
        <?php endforeach ?>
        <form action="" method="GET">
            <td><input name="level" value="" require></td>
            <td><input name="heso" value="" require></td>
            <td>
                <button type="submit" name="action" value="add">Them</button>
            </td>
        </form>
    </table>
</body>

</html>